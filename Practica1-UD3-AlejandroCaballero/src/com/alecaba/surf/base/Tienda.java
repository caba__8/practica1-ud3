package com.alecaba.surf.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tiendas", schema = "tiendasurf", catalog = "")
public class Tienda {
    private int id;
    private String web;
    private String nombreTienda;
    private String direccionTienda;
    private String descripcion;
    private Date fechaCreacion;
    private String emailTienda;
    private String telefonoTienda;
    private List<Visita> visitas;
    private List<Producto> productos;
    private TiendaProveedor proveedores;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "web")
    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    @Basic
    @Column(name = "nombre_tienda")
    public String getNombreTienda() {
        return nombreTienda;
    }

    public void setNombreTienda(String nombreTienda) {
        this.nombreTienda = nombreTienda;
    }

    @Basic
    @Column(name = "direccion_tienda")
    public String getDireccionTienda() {
        return direccionTienda;
    }

    public void setDireccionTienda(String direccionTienda) {
        this.direccionTienda = direccionTienda;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Basic
    @Column(name = "email_tienda")
    public String getEmailTienda() {
        return emailTienda;
    }

    public void setEmailTienda(String emailTienda) {
        this.emailTienda = emailTienda;
    }

    @Basic
    @Column(name = "telefono_tienda")
    public String getTelefonoTienda() {
        return telefonoTienda;
    }

    public void setTelefonoTienda(String telefonoTienda) {
        this.telefonoTienda = telefonoTienda;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return id == tienda.id &&
                Objects.equals(web, tienda.web) &&
                Objects.equals(nombreTienda, tienda.nombreTienda) &&
                Objects.equals(direccionTienda, tienda.direccionTienda) &&
                Objects.equals(descripcion, tienda.descripcion) &&
                Objects.equals(fechaCreacion, tienda.fechaCreacion) &&
                Objects.equals(emailTienda, tienda.emailTienda) &&
                Objects.equals(telefonoTienda, tienda.telefonoTienda);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, web, nombreTienda, direccionTienda, descripcion, fechaCreacion, emailTienda, telefonoTienda);
    }

    @OneToMany(mappedBy = "tienda")
    public List<Visita> getVisitas() {
        return visitas;
    }

    public void setVisitas(List<Visita> visitas) {
        this.visitas = visitas;
    }

    @ManyToMany
    @JoinTable(name = "tienda_producto", catalog = "", schema = "tiendasurf", joinColumns = @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false))
    public List<Producto> getProductos() {
        return productos;
    }

    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_tienda", nullable = false)
    public TiendaProveedor getProveedores() {
        return proveedores;
    }

    public void setProveedores(TiendaProveedor proveedores) {
        this.proveedores = proveedores;
    }
}
