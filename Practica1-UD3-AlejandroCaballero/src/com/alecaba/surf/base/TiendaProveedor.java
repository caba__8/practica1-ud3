package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tienda_proveedor", schema = "tiendasurf", catalog = "")
public class TiendaProveedor {
    private List<Tienda> tienda;
    private List<Proveedor> proveedores;
    private String id;

    @OneToMany(mappedBy = "proveedores")
    public List<Tienda> getTienda() {
        return tienda;
    }

    public void setTienda(List<Tienda> tienda) {
        this.tienda = tienda;
    }

    @ManyToMany(mappedBy = "tiendas")
    public List<Proveedor> getProveedores() {
        return proveedores;
    }

    public void setProveedores(List<Proveedor> proveedores) {
        this.proveedores = proveedores;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Id
    public String getId() {
        return id;
    }
}
