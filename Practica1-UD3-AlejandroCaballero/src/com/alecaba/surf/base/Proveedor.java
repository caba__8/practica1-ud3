package com.alecaba.surf.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "proveedores", schema = "tiendasurf", catalog = "")
public class Proveedor {
    private int id;
    private String nombreProveedor;
    private float precioProveedor;
    private Date fechaContratacion;
    private List<Proveedor> tiendas;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_proveedor")
    public String getNombreProveedor() {
        return nombreProveedor;
    }

    public void setNombreProveedor(String nombreProveedor) {
        this.nombreProveedor = nombreProveedor;
    }

    @Basic
    @Column(name = "precio_proveedor")
    public float getPrecioProveedor() {
        return precioProveedor;
    }

    public void setPrecioProveedor(float precioProveedor) {
        this.precioProveedor = precioProveedor;
    }

    @Basic
    @Column(name = "fecha_contratacion")
    public Date getFechaContratacion() {
        return fechaContratacion;
    }

    public void setFechaContratacion(Date fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Float.compare(proveedor.precioProveedor, precioProveedor) == 0 &&
                Objects.equals(nombreProveedor, proveedor.nombreProveedor) &&
                Objects.equals(fechaContratacion, proveedor.fechaContratacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreProveedor, precioProveedor, fechaContratacion);
    }

    @ManyToMany
    @JoinTable(name = "tienda_proveedor", catalog = "", schema = "tiendasurf", joinColumns = @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false))
    public List<Proveedor> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Proveedor> tiendas) {
        this.tiendas = tiendas;
    }
}
