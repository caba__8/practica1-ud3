package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "compra_producto", schema = "tiendasurf", catalog = "")
public class DetalleCompra {
    private int id;
    private int cantidad;
    private float precioCompra;
    private Producto producto;
    private Compra compra;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "precio_compra")
    public float getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(float precioCompra) {
        this.precioCompra = precioCompra;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DetalleCompra that = (DetalleCompra) o;
        return id == that.id &&
                cantidad == that.cantidad &&
                Float.compare(that.precioCompra, precioCompra) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, cantidad, precioCompra);
    }

    @ManyToOne
    @JoinColumn(name = "id_producto", referencedColumnName = "id", nullable = false)
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @ManyToOne
    @JoinColumn(name = "id_compra", referencedColumnName = "id", nullable = false)
    public Compra getCompra() {
        return compra;
    }

    public void setCompra(Compra compra) {
        this.compra = compra;
    }
}
