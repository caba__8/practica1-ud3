package com.alecaba.surf.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "diseñadores", schema = "tiendasurf", catalog = "")
public class Diseñador {
    private int id;
    private String nombreDiseñador;
    private Date fechaNacimientoDiseñador;
    private String paisDiseñador;
    private String emailDiseñador;
    private String telefonoDiseñador;
    private ProductoDiseñador productos;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre_diseñador")
    public String getNombreDiseñador() {
        return nombreDiseñador;
    }

    public void setNombreDiseñador(String nombreDiseñador) {
        this.nombreDiseñador = nombreDiseñador;
    }

    @Basic
    @Column(name = "fechaNacimiento_diseñador")
    public Date getFechaNacimientoDiseñador() {
        return fechaNacimientoDiseñador;
    }

    public void setFechaNacimientoDiseñador(Date fechaNacimientoDiseñador) {
        this.fechaNacimientoDiseñador = fechaNacimientoDiseñador;
    }

    @Basic
    @Column(name = "pais_diseñador")
    public String getPaisDiseñador() {
        return paisDiseñador;
    }

    public void setPaisDiseñador(String paisDiseñador) {
        this.paisDiseñador = paisDiseñador;
    }

    @Basic
    @Column(name = "email_diseñador")
    public String getEmailDiseñador() {
        return emailDiseñador;
    }

    public void setEmailDiseñador(String emailDiseñador) {
        this.emailDiseñador = emailDiseñador;
    }

    @Basic
    @Column(name = "telefono_diseñador")
    public String getTelefonoDiseñador() {
        return telefonoDiseñador;
    }

    public void setTelefonoDiseñador(String telefonoDiseñador) {
        this.telefonoDiseñador = telefonoDiseñador;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Diseñador diseñador = (Diseñador) o;
        return id == diseñador.id &&
                Objects.equals(nombreDiseñador, diseñador.nombreDiseñador) &&
                Objects.equals(fechaNacimientoDiseñador, diseñador.fechaNacimientoDiseñador) &&
                Objects.equals(paisDiseñador, diseñador.paisDiseñador) &&
                Objects.equals(emailDiseñador, diseñador.emailDiseñador) &&
                Objects.equals(telefonoDiseñador, diseñador.telefonoDiseñador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombreDiseñador, fechaNacimientoDiseñador, paisDiseñador, emailDiseñador, telefonoDiseñador);
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_diseñador", nullable = false)
    public ProductoDiseñador getProductos() {
        return productos;
    }

    public void setProductos(ProductoDiseñador productos) {
        this.productos = productos;
    }
}
