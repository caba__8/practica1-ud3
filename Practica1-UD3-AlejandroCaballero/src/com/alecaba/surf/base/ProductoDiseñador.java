package com.alecaba.surf.base;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "producto_diseñador", schema = "tiendasurf", catalog = "")
public class ProductoDiseñador {
    private Producto producto;
    private List<Diseñador> diseñador;
    private String id;

    @OneToOne(mappedBy = "diseñador")
    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    @OneToMany(mappedBy = "productos")
    public List<Diseñador> getDiseñador() {
        return diseñador;
    }

    public void setDiseñador(List<Diseñador> diseñador) {
        this.diseñador = diseñador;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Id
    public String getId() {
        return id;
    }
}
