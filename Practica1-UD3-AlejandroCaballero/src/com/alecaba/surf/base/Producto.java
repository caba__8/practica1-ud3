package com.alecaba.surf.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "productos", schema = "tiendasurf", catalog = "")
public class Producto {
    private int id;
    private String codigo;
    private String marca;
    private int talla;
    private String nombreProducto;
    private String descripcionProducto;
    private float precio;
    private Date fechaDiseño;
    private List<Tienda> tiendas;
    private List<DetalleCompra> detalles;
    private ProductoDiseñador diseñador;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "marca")
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    @Basic
    @Column(name = "talla")
    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    @Basic
    @Column(name = "nombre_producto")
    public String getNombreProducto() {
        return nombreProducto;
    }

    public void setNombreProducto(String nombreProducto) {
        this.nombreProducto = nombreProducto;
    }

    @Basic
    @Column(name = "descripcion_producto")
    public String getDescripcionProducto() {
        return descripcionProducto;
    }

    public void setDescripcionProducto(String descripcionProducto) {
        this.descripcionProducto = descripcionProducto;
    }

    @Basic
    @Column(name = "precio")
    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_diseño")
    public Date getFechaDiseño() {
        return fechaDiseño;
    }

    public void setFechaDiseño(Date fechaDiseño) {
        this.fechaDiseño = fechaDiseño;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return id == producto.id &&
                talla == producto.talla &&
                Float.compare(producto.precio, precio) == 0 &&
                Objects.equals(codigo, producto.codigo) &&
                Objects.equals(marca, producto.marca) &&
                Objects.equals(nombreProducto, producto.nombreProducto) &&
                Objects.equals(descripcionProducto, producto.descripcionProducto) &&
                Objects.equals(fechaDiseño, producto.fechaDiseño);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, marca, talla, nombreProducto, descripcionProducto, precio, fechaDiseño);
    }

    @ManyToMany(mappedBy = "productos")
    public List<Tienda> getTiendas() {
        return tiendas;
    }

    public void setTiendas(List<Tienda> tiendas) {
        this.tiendas = tiendas;
    }

    @OneToMany(mappedBy = "producto")
    public List<DetalleCompra> getDetalles() {
        return detalles;
    }

    public void setDetalles(List<DetalleCompra> detalles) {
        this.detalles = detalles;
    }

    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id_producto", nullable = false)
    public ProductoDiseñador getDiseñador() {
        return diseñador;
    }

    public void setDiseñador(ProductoDiseñador diseñador) {
        this.diseñador = diseñador;
    }
}
